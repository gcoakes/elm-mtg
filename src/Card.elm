module Card exposing
    ( Card
    , ImageUris
    , Legalities
    , SearchPage
    , decodeCard
    , decodeImageUris
    , decodeLegalities
    , decodeSearchPage
    )

import Dict exposing (Dict)
import Json.Decode as Decode
    exposing
        ( Decoder
        , bool
        , dict
        , field
        , float
        , int
        , list
        , map6
        , nullable
        , string
        )
import Json.Decode.Pipeline exposing (optional, required)


type alias SearchPage =
    { cards : List Card
    , hasMore : Bool
    , nextPage : Maybe String
    , totalCards : Maybe Int
    , warnings : Maybe (List String)
    }


type alias Card =
    { allParts : Maybe (List RelatedCard)
    , arenaId : Maybe Int
    , artistIds : List String
    , artist : Maybe String
    , booster : Bool
    , borderColor : String
    , cardBackId : String
    , cardFaces : Maybe (List CardFace)
    , cmc : Float
    , collectorNumber : String
    , colorIdentity : List String
    , colorIndicator : Maybe (List String)
    , colors : Maybe (List String)
    , digital : Bool
    , edhrecRank : Maybe Int
    , flavorText : Maybe String
    , foil : Bool
    , frameEffects : Maybe (List String)
    , frame : String
    , fullArt : Bool
    , games : List String
    , handModifier : Maybe String
    , highresImage : Bool
    , id : String
    , illustrationId : Maybe String
    , imageUris : Maybe ImageUris
    , lang : String
    , layout : String
    , legalities : Legalities
    , lifeModifier : Maybe String
    , loyalty : Maybe String
    , manaCost : Maybe String
    , mtgoFoilId : Maybe Int
    , mtgoId : Maybe Int
    , multiverseIds : Maybe (List Int)
    , name : String
    , nonfoil : Bool
    , object : String
    , oracleId : String
    , oracleText : Maybe String
    , oversized : Bool
    , power : Maybe String
    , previewedAt : Maybe String
    , previewSource : Maybe String
    , previewSourceUri : Maybe String
    , prices : Dict String (Maybe String)
    , printedName : Maybe String
    , printedText : Maybe String
    , printedTypeLine : Maybe String
    , printsSearchUri : String
    , promo : Bool
    , promoTypes : Maybe (List String)
    , purchaseUris : Dict String String
    , rarity : String
    , relatedUris : Dict String String
    , releasedAt : String
    , reprint : Bool
    , reserved : Bool
    , rulingsUri : String
    , scryfallSetUri : String
    , scryfallUri : String
    , setName : String
    , setSearchUri : String
    , set : String
    , setType : String
    , setUri : String
    , storySpotlight : Bool
    , tcgplayerId : Maybe Int
    , textless : Bool
    , toughness : Maybe String
    , typeLine : String
    , uri : String
    , variation : Bool
    , variationOf : Maybe String
    , watermark : Maybe String
    }


type alias RelatedCard =
    { id : String
    , object : String
    , component : String
    , name : String
    , typeLine : String
    , uri : String
    }


type alias CardFace =
    { artist : Maybe String
    , colorIndicator : Maybe (List String)
    , colors : Maybe (List String)
    , flavorText : Maybe String
    , illustrationId : Maybe String
    , imageUris : Maybe ImageUris
    , loyalty : Maybe String
    , manaCost : String
    , name : String
    , object : String
    , oracleText : Maybe String
    , power : Maybe String
    , printedName : Maybe String
    , printedTypeLine : Maybe String
    , toughness : Maybe String
    , typeLine : String
    , watermark : Maybe String
    }


type alias ImageUris =
    { artCrop : String
    , borderCrop : String
    , large : String
    , normal : String
    , png : String
    , small : String
    }


type alias Legalities =
    { brawl : String
    , commander : String
    , duel : String
    , future : String
    , historic : String
    , legacy : String
    , modern : String
    , oldschool : String
    , pauper : String
    , penny : String
    , pioneer : String
    , standard : String
    , vintage : String
    }


decodeSearchPage : Decoder SearchPage
decodeSearchPage =
    Decode.succeed SearchPage
        |> required "data" (list decodeCard)
        |> required "has_more" bool
        |> optional "next_page" (nullable string) Nothing
        |> optional "total_cards" (nullable int) Nothing
        |> optional "warnings" (nullable (list string)) Nothing


decodeCard : Decoder Card
decodeCard =
    Decode.succeed Card
        |> optional "all_parts" (nullable (list decodeRelatedCard)) Nothing
        |> optional "arena_id" (nullable int) Nothing
        |> required "artist_ids" (list string)
        |> optional "artist" (nullable string) Nothing
        |> required "booster" bool
        |> required "border_color" string
        |> required "card_back_id" string
        |> optional "card_faces" (nullable (list decodeCardFace)) Nothing
        |> required "cmc" float
        |> required "collector_number" string
        |> required "color_identity" (list string)
        |> optional "color_indicator" (nullable (list string)) Nothing
        |> optional "colors" (nullable (list string)) Nothing
        |> required "digital" bool
        |> optional "edhrec_rank" (nullable int) Nothing
        |> optional "flavor_text" (nullable string) Nothing
        |> required "foil" bool
        |> optional "frame_effects" (nullable (list string)) Nothing
        |> required "frame" string
        |> required "full_art" bool
        |> required "games" (list string)
        |> optional "hand_modifier" (nullable string) Nothing
        |> required "highres_image" bool
        |> required "id" string
        |> optional "illustration_id" (nullable string) Nothing
        |> optional "image_uris" (nullable decodeImageUris) Nothing
        |> required "lang" string
        |> required "layout" string
        |> required "legalities" decodeLegalities
        |> optional "life_modifier" (nullable string) Nothing
        |> optional "loyalty" (nullable string) Nothing
        |> optional "mana_cost" (nullable string) Nothing
        |> optional "mtgo_foil_id" (nullable int) Nothing
        |> optional "mtgo_id" (nullable int) Nothing
        |> optional "multiverse_ids" (nullable (list int)) Nothing
        |> required "name" string
        |> required "nonfoil" bool
        |> required "object" string
        |> required "oracle_id" string
        |> optional "oracle_text" (nullable string) Nothing
        |> required "oversized" bool
        |> optional "power" (nullable string) Nothing
        |> optional "preview.previewed_at" (nullable string) Nothing
        |> optional "preview.source" (nullable string) Nothing
        |> optional "preview.source_uri" (nullable string) Nothing
        |> required "prices" (dict (nullable string))
        |> optional "printed_name" (nullable string) Nothing
        |> optional "printed_text" (nullable string) Nothing
        |> optional "printed_type_line" (nullable string) Nothing
        |> required "prints_search_uri" string
        |> required "promo" bool
        |> optional "promo_types" (nullable (list string)) Nothing
        |> required "purchase_uris" (dict string)
        |> required "rarity" string
        |> required "related_uris" (dict string)
        |> required "released_at" string
        |> required "reprint" bool
        |> required "reserved" bool
        |> required "rulings_uri" string
        |> required "scryfall_set_uri" string
        |> required "scryfall_uri" string
        |> required "set_name" string
        |> required "set_search_uri" string
        |> required "set" string
        |> required "set_type" string
        |> required "set_uri" string
        |> required "story_spotlight" bool
        |> optional "tcgplayer_id" (nullable int) Nothing
        |> required "textless" bool
        |> optional "toughness" (nullable string) Nothing
        |> required "type_line" string
        |> required "uri" string
        |> required "variation" bool
        |> optional "variation_of" (nullable string) Nothing
        |> optional "watermark" (nullable string) Nothing


decodeRelatedCard : Decoder RelatedCard
decodeRelatedCard =
    Decode.succeed RelatedCard
        |> required "id" string
        |> required "object" string
        |> required "component" string
        |> required "name" string
        |> required "type_line" string
        |> required "uri" string


decodeCardFace : Decoder CardFace
decodeCardFace =
    Decode.succeed CardFace
        |> optional "artist" (nullable string) Nothing
        |> optional "color_indicator" (nullable (list string)) Nothing
        |> optional "colors" (nullable (list string)) Nothing
        |> optional "flavor_text" (nullable string) Nothing
        |> optional "illustration_id" (nullable string) Nothing
        |> optional "image_uris" (nullable decodeImageUris) Nothing
        |> optional "loyalty" (nullable string) Nothing
        |> required "mana_cost" string
        |> required "name" string
        |> required "object" string
        |> optional "oracle_text" (nullable string) Nothing
        |> optional "power" (nullable string) Nothing
        |> optional "printed_name" (nullable string) Nothing
        |> optional "printed_type_line" (nullable string) Nothing
        |> optional "toughness" (nullable string) Nothing
        |> required "type_line" string
        |> optional "watermark" (nullable string) Nothing


decodeImageUris : Decoder ImageUris
decodeImageUris =
    map6 ImageUris
        (field "art_crop" string)
        (field "border_crop" string)
        (field "large" string)
        (field "normal" string)
        (field "png" string)
        (field "small" string)


decodeLegalities : Decoder Legalities
decodeLegalities =
    Decode.succeed Legalities
        |> required "brawl" string
        |> required "commander" string
        |> required "duel" string
        |> required "future" string
        |> required "historic" string
        |> required "legacy" string
        |> required "modern" string
        |> required "oldschool" string
        |> required "pauper" string
        |> required "penny" string
        |> required "pioneer" string
        |> required "standard" string
        |> required "vintage" string
