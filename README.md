# elm-mtg

A web-based deck builder and search tool for the trading card game [Magic:
the Gathering](https://magic.wizards.com/en). It is entirely a frontend
client to consume data from [Scryfall's](https://scryfall.com/) amazing
[api](https://scryfall.com/docs/api). While it is mostly intended as an
educational exploration of the [Elm Programming
Language](https://elm-lang.org/), there are some unique features which are
not available in the normal Scryfall interface.

## Features

- Drag-n-drop deck builder
- Infinite scroll search results
- Easy access to powerful regex searches
