module Main exposing (main)

-- Display a random MTG card from scryfall.

import Browser
import Card exposing (Card, SearchPage, decodeSearchPage)
import Dict
import DnD
import File.Download as Download
import Form exposing (Form)
import Form.View
import Html
    exposing
        ( Attribute
        , Html
        , button
        , div
        , form
        , h2
        , img
        , input
        , label
        , text
        , textarea
        )
import Html.Attributes as Attr exposing (class, style)
import Html.Events as Events
import Http
import Json.Decode
import List
import String
import Url.Builder as B



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type alias Model =
    { cards : List Card
    , nextPage : Maybe String
    , search : Form.View.Model QueryFormValues
    , error : Maybe Http.Error
    , cardSize : Int
    , mainboard : Board
    , sideboard : Board
    , draggable : DnD.Draggable Area Card
    , lastQuery : Maybe String
    }


type alias QueryFormValues =
    { name : String
    , oracleText : String
    , type_ : String
    , color : String
    , format : String
    , useRegex : Bool
    , rawQuery : String
    }


type alias ScryfallQuery =
    { name : Maybe String
    , oracleText : Maybe String
    , type_ : Maybe String
    , color : Maybe String
    , format : Maybe String
    , useRegex : Bool
    , rawQuery : Maybe String
    }


type Area
    = MainBoardArea
    | SideBoardArea
    | SearchArea


type alias Board =
    Dict.Dict String BoardEntry


type alias BoardEntry =
    { card : Card
    , count : Int
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { cards = []
      , search =
            Form.View.idle
                { name = ""
                , oracleText = ""
                , type_ = ""
                , color = ""
                , format = ""
                , useRegex = False
                , rawQuery = ""
                }
      , nextPage = Nothing
      , error = Nothing
      , cardSize = 33
      , mainboard = Dict.empty
      , sideboard = Dict.empty
      , draggable = dnd.model
      , lastQuery = Nothing
      }
    , Cmd.none
    )


dnd =
    DnD.init DnDMsg OnDrop



-- UPDATE


type Msg
    = Search ScryfallQuery
    | FormChanged (Form.View.Model QueryFormValues)
    | GotResult (Result Http.Error SearchPage)
    | ScrollEvent ScrollInfo
    | ChangeCardSize String
    | OnDrop Area Card
    | DnDMsg (DnD.Msg Area Card)
    | DownloadDeckList DeckListFormat


type alias ScrollInfo =
    { scrollHeight : Float
    , scrollTop : Float
    , offsetHeight : Float
    }


type DeckListFormat
    = CardHoarder


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Search queryFields ->
            let
                query =
                    encodeScryfallQuery queryFields
            in
            ( { model
                | cards = []
                , nextPage = Nothing
                , lastQuery = Just query
              }
            , searchCards query
            )

        FormChanged searchValues ->
            ( { model | search = searchValues }, Cmd.none )

        GotResult result ->
            case result of
                Ok page ->
                    ( { model
                        | cards = model.cards ++ page.cards
                        , nextPage = page.nextPage
                      }
                    , Cmd.none
                    )

                Err err ->
                    ( { model | error = Just err }, Cmd.none )

        ScrollEvent { scrollHeight, scrollTop, offsetHeight } ->
            if (scrollHeight - scrollTop) <= offsetHeight then
                case model.nextPage of
                    Just nextPage ->
                        ( { model | nextPage = Nothing }
                        , fetchSearchPage nextPage
                        )

                    Nothing ->
                        ( model, Cmd.none )

            else
                ( model, Cmd.none )

        ChangeCardSize ratio ->
            ( { model
                | cardSize =
                    String.toInt ratio |> Maybe.withDefault 20
              }
            , Cmd.none
            )

        OnDrop area card ->
            case area of
                MainBoardArea ->
                    ( { model
                        | mainboard = updateBoard model.mainboard card
                      }
                    , Cmd.none
                    )

                SideBoardArea ->
                    ( { model | sideboard = updateBoard model.sideboard card }, Cmd.none )

                SearchArea ->
                    let
                        query =
                            String.join " "
                                [ encodeScryfallField QuoteLiteral "!" card.name
                                , "unique:prints"
                                ]
                    in
                    ( { model
                        | cards = []
                        , nextPage = Nothing
                        , lastQuery = Just query
                      }
                    , searchCards query
                    )

        DnDMsg msg_ ->
            ( { model | draggable = DnD.update msg_ model.draggable }, Cmd.none )

        DownloadDeckList fmt ->
            ( model, downloadDeckList fmt model )


downloadDeckList : DeckListFormat -> Model -> Cmd Msg
downloadDeckList fmt model =
    Download.string
        "decklist.txt"
        "text/plain"
        (case fmt of
            CardHoarder ->
                showCardHoarderFormat model.mainboard model.sideboard
        )


updateBoard : Board -> Card -> Board
updateBoard board card =
    case Dict.get card.id board of
        Nothing ->
            Dict.insert
                card.id
                { card = card, count = 1 }
                board

        Just entry ->
            Dict.insert
                card.id
                { card = card, count = entry.count + 1 }
                board



-- HTTP


scryfallApi : List String -> List B.QueryParameter -> String
scryfallApi path queries =
    B.crossOrigin
        "https://api.scryfall.com"
        path
        queries


searchCards : String -> Cmd Msg
searchCards query =
    scryfallApi [ "cards", "search" ] [ B.string "q" query ]
        |> fetchSearchPage


fetchSearchPage : String -> Cmd Msg
fetchSearchPage url =
    Http.get
        { url = url
        , expect = Http.expectJson GotResult decodeSearchPage
        }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    dnd.subscriptions model.draggable



-- VIEW


view : Model -> Html Msg
view model =
    div
        [ style "width" "100vw"
        , style "height" "100vh"
        , style "overflow" "hidden"
        , style "display" "flex"
        , style "flex-direction" "row"
        ]
        [ viewSidebar
            [ style "flex" "1 1 auto"
            , style "width" "25%"
            ]
            model
        , viewSearchResults
            [ style "flex" "1 1 auto"
            , style "width" "75%"
            ]
            model.cardSize
            model.cards
        ]


viewSidebar : List (Attribute Msg) -> Model -> Html Msg
viewSidebar attrs model =
    div
        ([ style "display" "flex"
         , style "flex-direction" "column"
         ]
            ++ attrs
        )
        [ DnD.dragged model.draggable viewDragged
        , dnd.droppable SearchArea
            []
            [ h2
                []
                [ text "Search:" ]
            , case model.lastQuery of
                Nothing ->
                    text ""

                Just query ->
                    div
                        []
                        [ label [] [ text "Most recent query:" ]
                        , textarea
                            [ style "width" "100%"
                            , style "resize" "none"
                            , Attr.readonly True
                            , Attr.value query
                            ]
                            []
                        ]
            , Form.View.asHtml
                { onChange = FormChanged
                , action = "Search"
                , loading = "Searching..."
                , validation = Form.View.ValidateOnSubmit
                }
                form
                model.search
            , div
                []
                [ label
                    []
                    [ text <| String.fromInt model.cardSize ++ "%" ]
                , input
                    [ Attr.type_ "range"
                    , Attr.min "1"
                    , Attr.max "100"
                    , Attr.value <| String.fromInt model.cardSize
                    , Events.onInput ChangeCardSize
                    , style "flex-grow" "1"
                    ]
                    []
                ]
            ]
        , div
            [ style "flex-grow" "1"
            , style "display" "flex"
            , style "flex-direction" "column"
            ]
            [ div
                [ style "display" "flex"
                , style "flex" "2 1 auto"
                , style "flex-direction" "column"
                ]
                [ h2 [] [ text "Mainboard:" ]
                , dnd.droppable MainBoardArea
                    [ style "height" "100%"
                    , style "width" "100%"
                    , style "flex" "1 1 auto"
                    ]
                    [ textarea
                        [ style "width" "100%"
                        , style "height" "100%"
                        , style "resize" "none"
                        , Attr.readonly True
                        , Attr.value <| showBoard model.mainboard
                        ]
                        []
                    ]
                ]
            , div
                [ style "display" "flex"
                , style "flex" "1 1 auto"
                , style "flex-direction" "column"
                ]
                [ h2 [] [ text "Sideboard:" ]
                , dnd.droppable SideBoardArea
                    [ style "flex" "1 1 auto"
                    ]
                    [ textarea
                        [ style "width" "100%"
                        , style "height" "100%"
                        , style "resize" "none"
                        , Attr.readonly True
                        , Attr.value <| showBoard model.sideboard
                        ]
                        []
                    ]
                ]
            , button
                [ Events.onClick (DownloadDeckList CardHoarder) ]
                [ text "Download" ]
            ]
        ]


viewDragged : Card -> Html m
viewDragged card =
    case card.imageUris of
        Nothing ->
            text ""

        Just imgs ->
            img [ Attr.src imgs.small ] []


form : Form QueryFormValues Msg
form =
    let
        nameField =
            Form.textField
                { parser = Ok
                , value = .name
                , update = \value values -> { values | name = value }
                , error = always Nothing
                , attributes =
                    { label = "Name:"
                    , placeholder = ""
                    }
                }

        oracleTextField =
            Form.textField
                { parser = Ok
                , value = .oracleText
                , update =
                    \oracleText values ->
                        { values | oracleText = oracleText }
                , error = always Nothing
                , attributes =
                    { label = "Oracle Text:"
                    , placeholder = ""
                    }
                }

        typeField =
            Form.textField
                { parser = Ok
                , value = .type_
                , update =
                    \type_ values ->
                        { values | type_ = type_ }
                , error = always Nothing
                , attributes =
                    { label = "Type:"
                    , placeholder = ""
                    }
                }

        colorField =
            Form.textField
                { parser = Ok
                , value = .color
                , update =
                    \color values ->
                        { values | color = color }
                , error = always Nothing
                , attributes =
                    { label = "Color:"
                    , placeholder = ""
                    }
                }

        possibleFormats =
            [ "Standard"
            , "Future"
            , "Historic"
            , "Pioneer"
            , "Modern"
            , "Legacy"
            , "Pauper"
            , "Vintage"
            , "Penny"
            , "Commander"
            , "Brawl"
            , "Duel"
            , "Oldschool"
            ]

        formatField =
            Form.selectField
                { parser = Ok
                , error = always Nothing
                , value = .format
                , update = \format values -> { values | format = format }
                , attributes =
                    { label = "Format:"
                    , placeholder = ""
                    , options =
                        List.map
                            (\v -> ( String.toLower v, v ))
                            possibleFormats
                    }
                }

        useRegexField =
            Form.checkboxField
                { parser = Ok
                , value = .useRegex
                , update = \useRegex values -> { values | useRegex = useRegex }
                , error = always Nothing
                , attributes =
                    { label = "Use Regex?" }
                }

        rawQueryField =
            Form.textField
                { parser = Ok
                , value = .rawQuery
                , update = \rawQuery values -> { values | rawQuery = rawQuery }
                , error = always Nothing
                , attributes =
                    { label = "Raw Query:"
                    , placeholder = ""
                    }
                }
    in
    Form.succeed
        (\name oracleText type_ color format useRegex rawQuery ->
            Search
                (ScryfallQuery
                    name
                    oracleText
                    type_
                    color
                    format
                    useRegex
                    rawQuery
                )
        )
        |> Form.append (Form.optional nameField)
        |> Form.append (Form.optional oracleTextField)
        |> Form.append (Form.optional typeField)
        |> Form.append (Form.optional colorField)
        |> Form.append (Form.optional formatField)
        |> Form.append useRegexField
        |> Form.append (Form.optional rawQueryField)


viewSearchResults : List (Attribute Msg) -> Int -> List Card -> Html Msg
viewSearchResults attrs cardSize cards =
    div
        ([ style "display" "flex"
         , style "overflow" "scroll"
         , style "flex-flow" "row wrap"
         , style "justify-content" "space-around"
         , style "align-items" "center"
         , onScroll ScrollEvent
         ]
            ++ attrs
        )
        (List.map
            (viewCard
                [ style "flex" "0 0 auto"
                , style "width" (String.fromInt cardSize ++ "%")
                ]
            )
            cards
        )


viewCard : List (Attribute Msg) -> Card -> Html Msg
viewCard attrs card =
    div
        (class "card" :: attrs)
        (case card.imageUris of
            Nothing ->
                []

            Just imageUris ->
                [ dnd.draggable card
                    []
                    [ img
                        [ Attr.src imageUris.normal
                        , style "width" "100%"
                        ]
                        []
                    ]
                ]
        )


onScroll : (ScrollInfo -> msg) -> Attribute msg
onScroll msg =
    Events.on "scroll" (Json.Decode.map msg decodeScrollInfo)



-- ENCODE/DECODE


encodeScryfallQuery : ScryfallQuery -> String
encodeScryfallQuery query =
    let
        quote =
            if query.useRegex then
                QuoteRegex

            else
                QuoteLiteral
    in
    String.join
        " "
        (List.filterMap
            (\v -> v)
            [ Maybe.map (encodeScryfallField quote "name:") query.name
            , Maybe.map (encodeScryfallField quote "oracle:") query.oracleText
            , Maybe.map (encodeScryfallField quote "type:") query.type_
            , Maybe.map (encodeScryfallField QuoteLiteral "color:") query.color
            , Maybe.map (encodeScryfallField QuoteLiteral "f:") query.format
            , query.rawQuery
            ]
        )


type ScryfallQuoteType
    = QuoteLiteral
    | QuoteRegex


encodeScryfallField : ScryfallQuoteType -> String -> String -> String
encodeScryfallField typ prefix str =
    case typ of
        QuoteLiteral ->
            if List.length (String.words str) == 1 then
                prefix ++ str

            else
                prefix ++ "\"" ++ str ++ "\""

        QuoteRegex ->
            prefix ++ "/" ++ String.replace "/" "\\/" str ++ "/"


showCardHoarderFormat : Board -> Board -> String
showCardHoarderFormat mainboard sideboard =
    String.join "\n\n" (List.map showBoard [ mainboard, sideboard ])


showBoard : Board -> String
showBoard deck =
    Dict.values deck
        |> List.sortBy (\ent -> ent.card.name)
        |> List.map showBoardEntry
        |> String.join "\n"


showBoardEntry : BoardEntry -> String
showBoardEntry entry =
    String.fromInt entry.count ++ " " ++ entry.card.name


decodeScrollInfo : Json.Decode.Decoder ScrollInfo
decodeScrollInfo =
    Json.Decode.map3 ScrollInfo
        (Json.Decode.at [ "target", "scrollHeight" ] Json.Decode.float)
        (Json.Decode.at [ "target", "scrollTop" ] Json.Decode.float)
        (Json.Decode.at [ "target", "offsetHeight" ] Json.Decode.float)
